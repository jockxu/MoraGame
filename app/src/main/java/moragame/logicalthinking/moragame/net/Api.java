package moragame.logicalthinking.moragame.net;

import cn.droidlover.xdroidmvp.net.XApi;

/**
 * Created by wanglei on 2016/12/31.
 */

public class Api {


    public static final String JAPI_BASE_URL = "http://dev.cnzhanhao.cn:8080/";
    public static final String WEBSOCKET_URL = "ws://ali.cnzhanhao.cn:8081/";


    private static LoginService loginService;


    public static LoginService getLoginService() {
        if (loginService == null) {
            synchronized (Api.class) {
                if (loginService == null) {
                    loginService = XApi.getInstance().getRetrofit(JAPI_BASE_URL, true).create(LoginService.class);
                }
            }
        }
        return loginService;
    }
}

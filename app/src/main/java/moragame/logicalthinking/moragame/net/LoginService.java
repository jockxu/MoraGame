package moragame.logicalthinking.moragame.net;


import io.reactivex.Flowable;
import moragame.logicalthinking.moragame.model.GameHallResults;
import moragame.logicalthinking.moragame.model.HomeResults;
import moragame.logicalthinking.moragame.model.LoginResults;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by wanglei on 2016/12/31.
 */

public interface LoginService {


    @POST("Medicine/login.html")
    Flowable<LoginResults> getLoginDataJson(@Body RequestBody body);

    @POST("Medicine/loginOut.html")
    Flowable<LoginResults> getLoginOutDataJson(@Body RequestBody body);

    @POST("Medicine/register.html")
    Flowable<LoginResults> getRegDataToPostJson(@Body RequestBody body);

    @GET("Medicine/loginOnline.html")
    Flowable<HomeResults> getOnLineToGet();

    @GET("Medicine/api/game/combatscore/add")
    Flowable<LoginResults> getInitScoreToGet(@Query("id") String id);

    @GET("Medicine/api/game/combatscore/query")
    Flowable<LoginResults> getqQueryScoreToGet(@Query("id") String id);

    @POST("Medicine/api/game/combatscore/update")
    Flowable<LoginResults> getUpdateScoreOutDataJson(@Body RequestBody body);


    // 创建房间
    @POST("Medicine/api/game/game/combathouse/add")
    Flowable<LoginResults> getNewRoomDataJson(@Body RequestBody body);

    // 查询房间信息
    @GET("Medicine/api/game/combathouse/query")
    Flowable<LoginResults> getQueryRoomInfoToGet(@Query("hid") String id);


    // 查询全部房间
    @GET("Medicine/api/game/combathouse/selectAll")
    Flowable<GameHallResults> getQueryAllRoomInfoToGet();

    // 查询用户所在房间信息
    @GET("Medicine/api/game/combathouse/querybyuserid")
    Flowable<LoginResults> getQueryUserRoomInfoToGet(@Query("uid") String id);


    // 加入房间
    @POST("Medicine/api/game/combathouse/update")
    Flowable<HomeResults> getAddRoomDataJson(@Body RequestBody body);


}

package moragame.logicalthinking.moragame.model;

/**
 * Created by wanglei on 2016/12/10.
 */

public class HomeResults extends BaseModel {


    private int code;
    private String msg;
    private String data;
    private String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}

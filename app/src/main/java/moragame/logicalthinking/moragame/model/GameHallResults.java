package moragame.logicalthinking.moragame.model;

import java.util.List;

/**
 * Created by wanglei on 2016/12/10.
 */

public class GameHallResults extends BaseModel {


    private int code;
    private String message;
    private List<Data> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }


    public class Data {
        private String roomId;
        private  boolean isOneSomeone; // 记录1号座位是否有人
        private  boolean isTwoSomeone; // 记录2号座位是否有人
        private List<String> users;

        public boolean isOneSomeone() {
            return isOneSomeone;
        }

        public void setOneSomeone(boolean oneSomeone) {
            isOneSomeone = oneSomeone;
        }

        public boolean isTwoSomeone() {
            return isTwoSomeone;
        }

        public void setTwoSomeone(boolean twoSomeone) {
            isTwoSomeone = twoSomeone;
        }

        public void setRoomId(String roomId) {
            this.roomId = roomId;
        }

        public String getRoomId() {
            return this.roomId;
        }

        public void setUsers(List<String> users) {
            this.users = users;
        }

        public List<String> getUsers() {
            return this.users;
        }
    }


}

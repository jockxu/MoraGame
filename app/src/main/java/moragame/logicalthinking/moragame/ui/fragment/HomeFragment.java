package moragame.logicalthinking.moragame.ui.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;

import cn.droidlover.xdroidmvp.kit.Kits;
import cn.droidlover.xdroidmvp.log.XLog;
import cn.droidlover.xdroidmvp.mvp.XLazyFragment;
import cn.droidlover.xdroidmvp.net.NetError;
import moragame.logicalthinking.moragame.R;
import moragame.logicalthinking.moragame.model.HomeResults;
import moragame.logicalthinking.moragame.model.LoginResults;
import moragame.logicalthinking.moragame.present.PHomePager;
import moragame.logicalthinking.moragame.ui.GameHallActivity;
import moragame.logicalthinking.moragame.ui.ManMachineActivity;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * 作者：yaochangliang on 2016/8/14 08:18
 * 邮箱：yaochangliang159@sina.com
 */
public class HomeFragment extends XLazyFragment<PHomePager> {
    @BindView(R.id.online_num)
    TextView mOnlineNum;        // 在线人数


    public static HomeFragment newInstance(String text) {
        HomeFragment fragmentCommon = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        fragmentCommon.setArguments(bundle);
        return fragmentCommon;
    }

    @Override
    public void initData(Bundle savedInstanceState) {

        if (Kits.NetWork.getNetworkTypeName(getActivity()) == Kits.NetWork.NETWORK_TYPE_DISCONNECT) {
            Toast.makeText(getActivity(), "网络没有连接", Toast.LENGTH_SHORT).show();
            return;
        }

        getP().getOnLine();

    }


    public void onLineData(HomeResults model) {
        XLog.d("在线人数" + model.getData());
        if (200 == model.getCode() && !TextUtils.isEmpty(model.getData())) {
            mOnlineNum.setText("在线人数" + model.getData());
        }

    }


    public void onNewRoom(LoginResults model) {
//        if (0 == model.getCode()) {
//            GameRoomActivity.launch(getActivity(), "1");
//        }
//
//        Toast.makeText(getActivity(), model.getMessage(), Toast.LENGTH_SHORT).show();
    }


    public void showError(NetError error) {
        XLog.d("错误代码" + error.getType() + "\n错误信息" + error.getMessage());
    }


    @OnClick({
            R.id.man_machinie,
            R.id.game_hall,
            R.id.new_room,
    })
    public void clickEvent(View view) {


        switch (view.getId()) {

            case R.id.man_machinie:

                ManMachineActivity.launch(getActivity());
                break;

            case R.id.game_hall:
                GameHallActivity.launch(getActivity());

                break;

            case R.id.new_room:  // 创建房间


//                JSONObject result = new JSONObject();
//                try {
//                    result.put("a_userid", SharedPrefUtils.getLoginID(getActivity()));
//
//                } catch (JSONException e) {
//
//                    e.printStackTrace();
//                }
//                RequestBody body = RequestBody.create(MediaType.parse("application/json"), result.toString());
//                getP().getNewRoom(body);
                break;
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public PHomePager newP() {
        return new PHomePager();
    }


}
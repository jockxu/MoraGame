package moragame.logicalthinking.moragame.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.OnClick;

import cn.droidlover.xdroidmvp.log.XLog;
import cn.droidlover.xdroidmvp.mvp.XLazyFragment;
import cn.droidlover.xdroidmvp.net.NetError;
import moragame.logicalthinking.moragame.R;
import moragame.logicalthinking.moragame.model.LoginResults;
import moragame.logicalthinking.moragame.present.PMyPager;
import moragame.logicalthinking.moragame.ui.JLoginActivity;
import moragame.logicalthinking.moragame.utils.DialogUtils;
import moragame.logicalthinking.moragame.utils.SharedPrefUtils;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * 作者：yaochangliang on 2016/8/14 08:18
 * 邮箱：yaochangliang159@sina.com
 */
public class MyFragment extends XLazyFragment<PMyPager> implements View.OnClickListener{

    DialogUtils mDialog;

    public static MyFragment newInstance(String text) {
        MyFragment fragmentCommon = new MyFragment();
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        fragmentCommon.setArguments(bundle);
        return fragmentCommon;
    }


    @Override
    public void initData(Bundle savedInstanceState) {
        mDialog =   new DialogUtils.Builder(getActivity()) .cancelTouchout(false).view(R.layout.dialog_login_out).widthpx(LinearLayout.LayoutParams.MATCH_PARENT).heightpx(LinearLayout.LayoutParams.MATCH_PARENT
        ).addViewOnclick(R.id.dialog_out_enter, this).addViewOnclick(R.id.dialog_out_cancel, this).style(R.style.dialog).build();
    }

    public void loginOutData(LoginResults model) {
        XLog.d("退出登录" + model.toString());
        if (200 == model.getCode()) {
            SharedPrefUtils.setLoginID(getActivity(),"0");
            JLoginActivity.launch(getActivity());
            getActivity().finish();
        }
    }

    public void showError(NetError error) {
        XLog.d("错误代码" + error.getType() + "\n错误信息" + error.getMessage());
    }

    @OnClick({
            R.id.login_out,

    })
    public void clickEvent(View view) {

        switch (view.getId()) {
            case R.id.login_out:  // 退出登录


                mDialog.show();

                break;
        }
    }


    private  void logOut(){
        JSONObject result = new JSONObject();
        try {
            result.put("userId",SharedPrefUtils.getLoginID(getActivity()));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), result.toString());
        getP().loginOut(body);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_my;
    }

    @Override
    public PMyPager newP() {
        return new PMyPager();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.dialog_out_enter:

                mDialog.dismiss();
                logOut();

                break;

            case R.id.dialog_out_cancel:
                mDialog.dismiss();
                break;
        }
    }
}
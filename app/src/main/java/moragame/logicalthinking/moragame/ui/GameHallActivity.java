package moragame.logicalthinking.moragame.ui;


import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;

import cn.droidlover.xdroidmvp.log.XLog;
import cn.droidlover.xdroidmvp.mvp.XActivity;
import cn.droidlover.xdroidmvp.net.NetError;
import cn.droidlover.xdroidmvp.router.Router;
import cn.droidlover.xrecyclerview.XRecyclerView;
import moragame.logicalthinking.moragame.R;
import moragame.logicalthinking.moragame.adapter.GameHallAdapter;
import moragame.logicalthinking.moragame.model.GameHallResults;
import moragame.logicalthinking.moragame.model.HomeResults;
import moragame.logicalthinking.moragame.present.PGameHallPager;
import moragame.logicalthinking.moragame.utils.SharedPrefUtils;
import okhttp3.MediaType;
import okhttp3.RequestBody;


/**
 * Created by wanglei on 2016/12/22.
 */

public class GameHallActivity extends XActivity<PGameHallPager> implements GameHallAdapter.OnImageClilckListener {


    @BindView(R.id.game_hall_recyclerview)
    XRecyclerView mRecyclerView;

    GameHallAdapter mAdapter;

    int pos;

    @Override
    public void bindEvent() {
        super.bindEvent();
        mRecyclerView.gridLayoutManager(this, 3).setAdapter(mAdapter = new GameHallAdapter(this));
        mAdapter.setOnImgClickListener(this);

    }

    @Override
    public void initData(Bundle savedInstanceState) {


    }


    @Override
    public void imgOne(GameHallResults.Data data, int postion) {
        pos = postion;
        if (data.isOneSomeone()) {
            Toast.makeText(this, "该座位已经有人了", Toast.LENGTH_SHORT).show();
            return;
        }
        addHall("1", data.getRoomId());

    }

    @Override
    protected void onResume() {
        super.onResume();
        getP().getAllHall();
    }

    @Override
    public void imgTwo(GameHallResults.Data data, int postion) {
        pos = postion;
        if (data.isTwoSomeone()) {
            Toast.makeText(this, "该座位已经有人了", Toast.LENGTH_SHORT).show();
            return;
        }
        addHall("1", data.getRoomId());
    }


    /**
     * @param opt    1 加入房间 2 退出房间
     * @param roomId 房间id
     */
    public void addHall(String opt, String roomId) {
        JSONObject result = new JSONObject();
        try {

            result.put("opt", opt);
            result.put("userId", SharedPrefUtils.getLoginID(this));
            result.put("roomId", roomId);

        } catch (JSONException e) {


            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), result.toString());
        getP().getAddReturnHall(body);

    }


    public void onAllHall(GameHallResults model) {
        XLog.d("全部房间" + model.getCode());
        if (0 == model.getCode() && model.getData() != null) {
            mAdapter.clearData();
            mAdapter.addData(model.getData());

        } else {

            Toast.makeText(this, model.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void onAddReturnHall(HomeResults model) {

        XLog.d("加入退出房间" + model.getCode());
        if (0 == model.getCode()) {
            GameRoomActivity.launch(this, mAdapter.getDataSource().get(pos).getRoomId());

        } else {

            Toast.makeText(this, model.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void showError(NetError error) {
        XLog.d("错误代码" + error.getType() + "\n错误信息" + error.getMessage());
    }

    public static void launch(Activity activity) {
        Router.newIntent(activity)
                .to(GameHallActivity.class)
                .launch();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_game_hall;
    }


    @Override
    public PGameHallPager newP() {
        return new PGameHallPager();
    }

}

package moragame.logicalthinking.moragame.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;

import cn.droidlover.xdroidmvp.log.XLog;
import cn.droidlover.xdroidmvp.mvp.XActivity;
import cn.droidlover.xdroidmvp.net.NetError;
import cn.droidlover.xdroidmvp.router.Router;
import moragame.logicalthinking.moragame.R;
import moragame.logicalthinking.moragame.model.LoginResults;
import moragame.logicalthinking.moragame.present.PLoginPager;
import moragame.logicalthinking.moragame.utils.DialogUtils;
import moragame.logicalthinking.moragame.utils.SharedPrefUtils;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by wanglei on 2016/12/22.
 */

public class JLoginActivity extends XActivity<PLoginPager> {


    @BindView(R.id.user_name)
    EditText mUserName;
    @BindView(R.id.user_pwd)
    EditText mUserPwd;

    DialogUtils mDialog;

    @Override
    public void initData(Bundle savedInstanceState) {

        mDialog = new DialogUtils.Builder(this).cancelTouchout(false).view(R.layout.dialog_loading).widthpx(LinearLayout.LayoutParams.MATCH_PARENT).heightpx(LinearLayout.LayoutParams.MATCH_PARENT
        ).style(R.style.dialog).build();

    }


    @OnClick({
            R.id.user_login,
            R.id.user_reg
    })
    public void clickEvent(View view) {

        String name = mUserName.getText().toString();
        String pwd = mUserPwd.getText().toString();
        switch (view.getId()) {

            case R.id.user_login:


                XLog.d("登录用户名" + name + "密码" + pwd);


                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(pwd)) {
                    Toast.makeText(this, "用户名或密码不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }

                mDialog.show();
                login(name, pwd);


                break;

            case R.id.user_reg:
                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(pwd)) {
                    Toast.makeText(this, "用户名或密码不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (name.length() > 8) {
                    Toast.makeText(this, "用户名长度不能超过8位", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (pwd.length() > 16) {
                    Toast.makeText(this, "密码长度不能超过16位", Toast.LENGTH_SHORT).show();
                    return;
                }
                XLog.d("注册用户名" + name + "密码" + pwd);
                mDialog.show();
                reg(name, pwd);
                break;
        }
    }

    // 登录
    private void login(String name, String pwd) {

        mDialog.dismiss();
        JSONObject result = new JSONObject();
        try {
            result.put("username", name);
            result.put("userpwd", pwd);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), result.toString());
        getP().login(body);
    }

    //注册
    private void reg(String name, String pwd) {
        mDialog.dismiss();
        JSONObject result = new JSONObject();
        try {
            result.put("username", name);
            result.put("userpwd", pwd);
        } catch (JSONException e) {


            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), result.toString());
        getP().reg(body);
    }


    public void showError(NetError error) {
        mDialog.dismiss();
        XLog.d("错误代码" + error.getType() + "\n错误信息" + error.getMessage());
    }

    public void loginData(LoginResults model) {
        XLog.d("用户信息" + model.toString());
        if (200 == model.getCode()) {
            //保存登录用户id
            if (model.getData() != null) {
                SharedPrefUtils.setLoginID(this, model.getData().getUserid());
                JMainActivity.launch(this);
                finish();
            } else {
                Toast.makeText(this, model.getMsg(), Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(this, model.getMsg(), Toast.LENGTH_SHORT).show();
        }

    }

    public void regData(LoginResults model) {
        XLog.d("注册信息" + model.toString());

        if (200 == model.getCode()) {

            if (model.getData() != null && !TextUtils.isEmpty(model.getData().getUserid())) {
                getP().initScore(model.getData().getUserid());
            }
            Toast.makeText(this, model.getMsg(), Toast.LENGTH_SHORT).show();

        }
    }

    public void initScore(LoginResults model) {
        XLog.d("初始化" + model.toString());
    }

    public static void launch(Activity activity) {
        Router.newIntent(activity)
                .to(JLoginActivity.class)
                .launch();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_jlogin;
    }


    @Override
    public PLoginPager newP() {
        return new PLoginPager();
    }
}

package moragame.logicalthinking.moragame.ui;


import android.os.Bundle;
import android.os.Handler;


import cn.droidlover.xdroidmvp.mvp.XActivity;
import moragame.logicalthinking.moragame.R;
import moragame.logicalthinking.moragame.utils.SharedPrefUtils;


/**
 * Created by wanglei on 2016/12/22.
 */

public class StartActivity extends XActivity {


    @Override
    public void initData(Bundle savedInstanceState) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (Integer.parseInt(SharedPrefUtils.getLoginID(StartActivity.this)) > 0) {
                    JMainActivity.launch(StartActivity.this);
                } else {
                    JLoginActivity.launch(StartActivity.this);
                }


                finish();
            }
        }, 2000);


    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_start;
    }


    @Override
    public Object newP() {
        return null;
    }
}

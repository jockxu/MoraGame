package moragame.logicalthinking.moragame.ui.fragment;

import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;

import cn.droidlover.xdroidmvp.mvp.XLazyFragment;
import moragame.logicalthinking.moragame.R;

/**
 * 作者：yaochangliang on 2016/8/14 08:18
 * 邮箱：yaochangliang159@sina.com
 */
public class MessageFragment extends XLazyFragment {



    public static MessageFragment newInstance(String text) {
        MessageFragment fragmentCommon = new MessageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        fragmentCommon.setArguments(bundle);
        return fragmentCommon;
    }

    @Override
    public void initData(Bundle savedInstanceState) {

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_message;
    }

    @Override
    public Object newP() {
        return null;
    }
}
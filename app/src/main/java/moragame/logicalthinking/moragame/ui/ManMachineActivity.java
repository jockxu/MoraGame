package moragame.logicalthinking.moragame.ui;


import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;

import cn.droidlover.xdroidmvp.kit.Kits;
import cn.droidlover.xdroidmvp.log.XLog;
import cn.droidlover.xdroidmvp.mvp.XActivity;
import cn.droidlover.xdroidmvp.net.NetError;
import cn.droidlover.xdroidmvp.router.Router;
import moragame.logicalthinking.moragame.R;
import moragame.logicalthinking.moragame.model.HomeResults;
import moragame.logicalthinking.moragame.model.LoginResults;
import moragame.logicalthinking.moragame.present.PManMachinePager;
import moragame.logicalthinking.moragame.utils.SharedPrefUtils;
import okhttp3.MediaType;
import okhttp3.RequestBody;


/**
 * Created by wanglei on 2016/12/22.
 */

public class ManMachineActivity extends XActivity<PManMachinePager> {


    @BindView(R.id.online_num)
    TextView mOnlineNum;        // 在线人数
    @BindView(R.id.tatol_num)
    TextView mTatolNum;        // 总局数
    @BindView(R.id.win_num)
    TextView mWinNum;       // 胜局数
    @BindView(R.id.draw_num)
    TextView mDrawNum;       // 平局
    @BindView(R.id.loss_num)
    TextView mLossNum;       // 负局数

    @BindView(R.id.img)
    ImageView mImg;       //  显示输赢

    int total;
    int win;
    int draw;
    int loss;

    @Override
    public void initData(Bundle savedInstanceState) {

        if (Kits.NetWork.getNetworkTypeName(this) == Kits.NetWork.NETWORK_TYPE_DISCONNECT) {
            Toast.makeText(this, "网络没有连接", Toast.LENGTH_SHORT).show();
            return;
        }

        getP().getOnLine();
        getP().queryScore(SharedPrefUtils.getLoginID(this));


    }


    public void onLineData(HomeResults model) {
        XLog.d("在线人数" + model.getData());
        if (200 == model.getCode() && !TextUtils.isEmpty(model.getData())) {
            mOnlineNum.setText("在线人数" + model.getData());
        }

    }

    public void queryScore(LoginResults model) {
        XLog.d("总局数" + model.toString());
        if (0 == model.getCode() && model.getData() != null) {
            total = model.getData().getTotal();
            win = model.getData().getWin();
            draw = model.getData().getDraw();
            loss = model.getData().getLoss();

            mTatolNum.setText("总  局  数" + total);
            mWinNum.setText("胜  局  数" + win);
            mDrawNum.setText("平  局  数" + draw);
            mLossNum.setText("负  局  数" + loss);

        }

    }

    public void updateScore(LoginResults model) {

        XLog.d("更新分数" + model.toString());

    }

    public void showError(NetError error) {
        XLog.d("错误代码" + error.getType() + "\n错误信息" + error.getMessage());
    }


    @OnClick({
            R.id.shitou,
            R.id.jaindao,
            R.id.bu,
    })
    public void clickEvent(View view) {

        int random = Kits.Random.getRandom(3);   // 获取随机数 0 表示石头 1 表示剪刀  2 表示布
        XLog.d("随机数" + random);

        switch (view.getId()) {

            case R.id.shitou:
                update(startGame(0, random) + "");
                break;

            case R.id.jaindao:
                update(startGame(1, random) + "");
                break;

            case R.id.bu:
                update(startGame(2, random) + "");
                break;
        }
    }

    private void update(String code) {
        JSONObject result = new JSONObject();
        try {
            result.put("id", SharedPrefUtils.getLoginID(this));
            result.put("code", code);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), result.toString());
        getP().updateScore(body);
    }


    /**
     * 开始游戏
     *
     * @param my     我出的拳
     * @param random 系统出的拳
     * @return 1输了  2 平局 0 赢了
     */
    private int startGame(int my, int random) {

        total++;
        mTatolNum.setText("总  局  数" + total);

        switch (my) {
            case 0:  // 石头
                if (random == 0) {
                    draw++;
                    mDrawNum.setText("平  局  数" + draw);
                    mImg.setBackgroundResource(R.drawable.gamepj_st);
                    return 2;
                } else if (random == 1) {
                    win++;
                    mWinNum.setText("胜  局  数" + win);
                    mImg.setBackgroundResource(R.drawable.gamewin_st);
                    return 0;
                } else if (random == 2) {
                    loss++;
                    mLossNum.setText("负  局  数" + loss);
                    mImg.setBackgroundResource(R.drawable.gamelose_st);
                    return 1;
                }
            case 1:     // 剪刀
                if (random == 0) {
                    loss++;
                    mLossNum.setText("负  局  数" + loss);
                    mImg.setBackgroundResource(R.drawable.gamelose_jd);
                    return 1;
                } else if (random == 1) {
                    draw++;
                    mDrawNum.setText("平  局  数" + draw);
                    mImg.setBackgroundResource(R.drawable.gamepj_jd);
                    return 2;
                } else if (random == 2) {
                    win++;
                    mWinNum.setText("胜  局  数" + win);
                    mImg.setBackgroundResource(R.drawable.gamewin_jd);
                    return 0;
                }
            case 2:    // 布
                if (random == 0) {
                    win++;
                    mWinNum.setText("胜  局  数" + win);
                    mImg.setBackgroundResource(R.drawable.gamewin_b);
                    return 0;
                } else if (random == 1) {
                    loss++;
                    mLossNum.setText("负  局  数" + loss);
                    mImg.setBackgroundResource(R.drawable.gamelose_b);
                    return 1;
                } else if (random == 2) {
                    draw++;
                    mDrawNum.setText("平  局  数" + draw);
                    mImg.setBackgroundResource(R.drawable.gamepj_b);
                    return 2;
                }
            default:
                mImg.setBackgroundResource(R.drawable.initgame);
                return 0;
        }
    }


    public static void launch(Activity activity) {
        Router.newIntent(activity)
                .to(ManMachineActivity.class)
                .launch();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_man_machine;
    }


    @Override
    public PManMachinePager newP() {
        return new PManMachinePager();
    }
}

package moragame.logicalthinking.moragame.ui;

import android.app.Activity;
import android.os.Bundle;

import com.ycl.tabview.library.TabView;
import com.ycl.tabview.library.TabViewChild;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import cn.droidlover.xdroidmvp.mvp.XActivity;
import cn.droidlover.xdroidmvp.router.Router;
import moragame.logicalthinking.moragame.R;
import moragame.logicalthinking.moragame.ui.fragment.HomeFragment;
import moragame.logicalthinking.moragame.ui.fragment.MessageFragment;
import moragame.logicalthinking.moragame.ui.fragment.MyFragment;

/**
 * Created by wanglei on 2016/12/22.
 */

public class JMainActivity extends XActivity {


    @BindView(R.id.tabView)
    TabView mTabView;


    @Override
    public void initData(Bundle savedInstanceState) {

        List<TabViewChild> tabViewChildList = new ArrayList<>();
        TabViewChild tabViewChild01 = new TabViewChild(R.drawable.click_home_icon, R.drawable.home_icon, "首页", HomeFragment.newInstance("首页"));
        TabViewChild tabViewChild02 = new TabViewChild(R.drawable.click_news_icon, R.drawable.news_icon, "消息", MessageFragment.newInstance("消息"));
        TabViewChild tabViewChild03 = new TabViewChild(R.drawable.click_my, R.drawable.my, "我的", MyFragment.newInstance("我的"));
        tabViewChildList.add(tabViewChild01);
        tabViewChildList.add(tabViewChild02);

        tabViewChildList.add(tabViewChild03);
        mTabView.setTabViewChild(tabViewChildList, getSupportFragmentManager());
    }


    public static void launch(Activity activity) {
        Router.newIntent(activity)
                .to(JMainActivity.class)
                .launch();
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_jmain;
    }


    @Override
    public Object newP() {
        return null;
    }
}

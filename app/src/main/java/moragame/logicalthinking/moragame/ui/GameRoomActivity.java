package moragame.logicalthinking.moragame.ui;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import butterknife.BindView;
import butterknife.OnClick;

import cn.droidlover.xdroidmvp.kit.Kits;
import cn.droidlover.xdroidmvp.log.XLog;
import cn.droidlover.xdroidmvp.mvp.XActivity;
import cn.droidlover.xdroidmvp.net.NetError;
import cn.droidlover.xdroidmvp.router.Router;
import moragame.logicalthinking.moragame.R;
import moragame.logicalthinking.moragame.model.HomeResults;
import moragame.logicalthinking.moragame.model.LoginResults;
import moragame.logicalthinking.moragame.present.PGameRoomPager;
import moragame.logicalthinking.moragame.utils.SharedPrefUtils;
import moragame.logicalthinking.moragame.utils.WebSocketUtils;
import okhttp3.MediaType;
import okhttp3.RequestBody;


/**
 * Created by wanglei on 2016/12/22.
 */

public class GameRoomActivity extends XActivity<PGameRoomPager> {


    private static final String ROOM_NO = "roomNo";
    public static final int START_INFO = 0x001;
    public static final int AGAINST_INFO = 0x002;
    public static final int IS_OPEN = 0x003;


    @BindView(R.id.game_room_no)
    TextView mTitleRoomNo;        // 标题房间号

    @BindView(R.id.user_info)
    TextView mUserInfo;        // 对手用户Id
    @BindView(R.id.user_state)
    TextView mUserState;        // 对手准备状态
    @BindView(R.id.user_a_score)
    TextView mASCore;        //  我的分数
    @BindView(R.id.user_b_score)
    TextView mBScore;        //对手分数
    @BindView(R.id.prepare_state)
    TextView mPrepareState;        // 我的准备状态

    @BindView(R.id.start_game)
    TextView mStartGame;

    @BindView(R.id.shitou)
    TextView mShiTou;
    @BindView(R.id.jaindao)
    TextView mJianDao;
    @BindView(R.id.bu)
    TextView mBu;

    @BindView(R.id.img)
    ImageView mImg;       //  显示输赢

    @BindView(R.id.ll_mora)
    LinearLayout mLLMora;// 我的准备状态显示
    @BindView(R.id.ll_socre)
    LinearLayout mLLSore; // 比分显示

    @BindView(R.id.ll_start)
    LinearLayout mLLStart; // 出拳按钮显示

    WebSocketUtils socketUtils;
    String roomId;

    Handler mHandler;

    String code; //对方出拳信息
    String myCode; // 我的出拳信息
    boolean isMor = false;
    int aWin;
    int bWin;

    boolean isConnect = false;

    @Override
    public void bindEvent() {
        super.bindEvent();
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case START_INFO:
                        XLog.d("准备返回" + msg.obj.toString());
                        if ("@heart".equals(msg.obj.toString()))
                            return;
                        try {
                            JSONObject jb = new JSONObject(msg.obj.toString());
                            String flag = jb.getString("flag");
                            JSONObject data = jb.getJSONObject("data");
                            String rid = data.getString("roomId");
                            XLog.d("房间id" + rid);
                            String userId = data.getString("userId");
                            XLog.d("对手" + userId);
                            if (!roomId.equals(rid))
                                return;
                            if (!data.isNull("code")) {


                                if (!userId.equals(SharedPrefUtils.getLoginID(GameRoomActivity.this))) {
                                    code = data.getString("code");

                                    XLog.d("对方出拳" + code + "我的出拳" + myCode);
                                    if (!TextUtils.isEmpty(code) && !TextUtils.isEmpty(myCode)) {
                                        update(startGame(Integer.parseInt(myCode), Integer.parseInt(code)) + "");
                                        qiYong();
                                        code = "";
                                    }
                                }
                            }

                            if (!userId.equals(SharedPrefUtils.getLoginID(GameRoomActivity.this))) {

                                mUserInfo.setVisibility(View.VISIBLE);
                                mLLMora.setVisibility(View.GONE);
                                mLLStart.setVisibility(View.VISIBLE);
                                mUserInfo.setText("玩家" + userId);
                                mUserState.setText("状态：已准备");
                                XLog.d("flag" + flag);
                                if ("1".equals(flag)) {
                                    senStartInfo();
                                }
                                if ("3".equals(flag)) {
                                    senStartInfo();
                                    mLLMora.setVisibility(View.VISIBLE);
                                    mLLStart.setVisibility(View.GONE);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            XLog.d("错误" + e.toString());
                        }

                        break;
                    case IS_OPEN:
                        XLog.d("socket状态" + msg.obj.toString());
                        if ("open".equals(msg.obj.toString())) {
                            if (!isConnect)
                                senStartInfo();
                        }
                        if ("close".equals(msg.obj.toString())) {
                            try {
                                isConnect = true;
                                socketUtils = new WebSocketUtils(mHandler);
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                            socketUtils.connect();
                        }

                        break;


                }
            }
        };


    }

    @Override
    public void initData(Bundle savedInstanceState) {
        try {
            socketUtils = new WebSocketUtils(mHandler);
            socketUtils.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        roomId = getIntent().getStringExtra(ROOM_NO);
        mTitleRoomNo.setText("房间号：" + roomId);


    }


    private void update(String code) {
        JSONObject result = new JSONObject();
        try {
            result.put("id", SharedPrefUtils.getLoginID(this));
            result.put("code", code);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), result.toString());
        getP().updateScore(body);
    }


    /**
     * 开始游戏
     *
     * @param my     我出的拳
     * @param random 系统出的拳
     * @return 1输了  2 平局 0 赢了
     */
    private int startGame(int my, int random) {


        mLLSore.setVisibility(View.VISIBLE);
        switch (my) {
            case 0:  // 石头
                if (random == 0) {

                    mImg.setBackgroundResource(R.drawable.gamepj_st);
                    return 2;
                } else if (random == 1) {
                    aWin++;
                    mASCore.setText(aWin + "");
                    mImg.setBackgroundResource(R.drawable.gamewin_st);
                    return 0;
                } else if (random == 2) {
                    bWin++;
                    mBScore.setText(bWin + "");
                    mImg.setBackgroundResource(R.drawable.gamelose_st);
                    return 1;
                }
            case 1:     // 剪刀
                if (random == 0) {
                    bWin++;
                    mBScore.setText(bWin + "");
                    mImg.setBackgroundResource(R.drawable.gamelose_jd);
                    return 1;
                } else if (random == 1) {

                    mImg.setBackgroundResource(R.drawable.gamepj_jd);
                    return 2;
                } else if (random == 2) {
                    aWin++;
                    mASCore.setText(aWin + "");
                    mImg.setBackgroundResource(R.drawable.gamewin_jd);
                    return 0;
                }
            case 2:    // 布
                if (random == 0) {
                    aWin++;
                    mASCore.setText(aWin + "");
                    mImg.setBackgroundResource(R.drawable.gamewin_b);
                    return 0;
                } else if (random == 1) {
                    bWin++;
                    mBScore.setText(bWin + "");
                    mImg.setBackgroundResource(R.drawable.gamelose_b);
                    return 1;
                } else if (random == 2) {

                    mImg.setBackgroundResource(R.drawable.gamepj_b);
                    return 2;
                }
            default:
                mImg.setBackgroundResource(R.drawable.initgame);
                return 0;
        }
    }

    public void senStartInfo() {
        JSONObject result = new JSONObject();
        try {
            JSONObject data = new JSONObject();
            data.put("roomId", roomId);
            data.put("userId", SharedPrefUtils.getLoginID(this));
            result.put("flag", "1");
            result.put("data", data);


            XLog.d("准备参数" + result.toString());

            socketUtils.send(result.toString());

        } catch (JSONException e) {


            e.printStackTrace();
        }


    }

    public void moraInfo(int code) {
        JSONObject result = new JSONObject();
        try {
            JSONObject data = new JSONObject();
            data.put("roomId", roomId);
            data.put("userId", SharedPrefUtils.getLoginID(this));
            data.put("code", code + "");
            result.put("flag", "2");
            result.put("data", data);


            XLog.d("出拳参数" + result.toString());

            socketUtils.send(result.toString());

        } catch (JSONException e) {


            e.printStackTrace();
        }

    }

    public void returRoom() {
        JSONObject result = new JSONObject();
        try {
            JSONObject data = new JSONObject();
            data.put("roomId", roomId);
            data.put("userId", SharedPrefUtils.getLoginID(this));
            result.put("flag", "3");
            result.put("data", data);


            XLog.d("准备参数" + result.toString());

            socketUtils.send(result.toString());

        } catch (JSONException e) {


            e.printStackTrace();
        }

    }

    public void onAddReturnHall(HomeResults model) {

        XLog.d("退出房间" + model.getMessage());
        if (0 == model.getCode()) {
            finish();

        } else {

            Toast.makeText(this, model.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void updateScore(LoginResults model) {

        XLog.d("更新分数" + model.toString());

    }

    public void showError(NetError error) {
        XLog.d("错误代码" + error.getType() + "\n错误信息" + error.getMessage());
    }


    private void jinYong() {

        mShiTou.setClickable(false);
        mShiTou.setBackgroundResource(R.color.text_999999);
        mJianDao.setClickable(false);
        mJianDao.setBackgroundResource(R.color.text_999999);
        mBu.setClickable(false);
        mBu.setBackgroundResource(R.color.text_999999);
    }


    private void qiYong() {

        mShiTou.setClickable(true);
        mShiTou.setBackgroundResource(R.color.colorPrimary);
        mJianDao.setClickable(true);
        mJianDao.setBackgroundResource(R.color.colorPrimary);
        mBu.setClickable(true);
        mBu.setBackgroundResource(R.color.colorPrimary);
    }

    @OnClick({
            R.id.shitou,
            R.id.jaindao,
            R.id.bu,

    })
    public void clickEvent(View view) {

        if (TextUtils.isEmpty(code)) {
            jinYong();
        }


        switch (view.getId()) {


            case R.id.shitou:
                myCode = 0 + "";
                moraInfo(0);
                //  update(startGame(0, random) + "");
                break;

            case R.id.jaindao:
                myCode = 1 + "";
                moraInfo(1);

                // update(startGame(1, random) + "");
                break;

            case R.id.bu:
                myCode = 2 + "";
                moraInfo(2);
                //  update(startGame(2, random) + "");
                break;
        }
    }


    /**
     * @param opt    1 加入房间 2 退出房间
     * @param roomId 房间id
     */
    public void returnHall(String opt, String roomId) {
        JSONObject result = new JSONObject();
        try {

            result.put("opt", opt);
            result.put("userId", SharedPrefUtils.getLoginID(this));
            result.put("roomId", roomId);

        } catch (JSONException e) {


            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), result.toString());
        getP().getAddReturnHall(body);

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        returnHall("2", roomId);
        returRoom();
        socketUtils.close();

    }

    public static void launch(Activity activity, String roomNo) {
        Router.newIntent(activity)
                .to(GameRoomActivity.class).putString(ROOM_NO, roomNo)
                .launch();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_game_room;
    }


    @Override
    public PGameRoomPager newP() {
        return new PGameRoomPager();
    }
}

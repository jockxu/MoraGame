package moragame.logicalthinking.moragame.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import cn.droidlover.xdroidmvp.base.SimpleRecAdapter;
import cn.droidlover.xdroidmvp.kit.KnifeKit;
import moragame.logicalthinking.moragame.R;
import moragame.logicalthinking.moragame.model.GameHallResults;
import moragame.logicalthinking.moragame.model.GameHallResults.Data;

/**
 * Created by wanglei on 2016/12/10.
 */

public class GameHallAdapter extends SimpleRecAdapter<Data, GameHallAdapter.ViewHolder> {


    OnImageClilckListener listener;

    public GameHallAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder newViewHolder(View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    public int getLayoutId() {
        return R.layout.adapter_game_hall;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final GameHallResults.Data model = data.get(position);
        holder.mHallNo.setText(model.getRoomId() + "号房间");
        if (model.getUsers() != null && model.getUsers().size() > 0) {
            holder.mWanjiaOne.setText("玩家：" + model.getUsers().get(0));
            model.setOneSomeone(true);
            if (model.getUsers().size() > 1) {
                holder.mWanjiaTwo.setText("玩家：" + model.getUsers().get(1));
                model.setTwoSomeone(true);

            }
        }

        holder.mAddImgOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.imgOne(model, position);
            }
        });
        holder.mAddImgTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.imgTwo(model, position);
            }
        });


    }

    public void setOnImgClickListener(OnImageClilckListener l) {
        this.listener = l;

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.game_hall_wanjia1)
        TextView mWanjiaOne;

        @BindView(R.id.game_hall_wanjia2)
        TextView mWanjiaTwo;
        @BindView(R.id.game_hall_no)
        TextView mHallNo;

        @BindView(R.id.img_one)
        ImageView mAddImgOne;

        @BindView(R.id.img_two)
        ImageView mAddImgTwo;

        public ViewHolder(View itemView) {
            super(itemView);
            KnifeKit.bind(this, itemView);
        }
    }

    public interface OnImageClilckListener {

        void imgOne(Data data, int postion);

        void imgTwo(Data data, int postion);
    }
}

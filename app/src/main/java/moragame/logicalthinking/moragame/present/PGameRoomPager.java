package moragame.logicalthinking.moragame.present;


import cn.droidlover.xdroidmvp.mvp.XPresent;
import cn.droidlover.xdroidmvp.net.ApiSubscriber;
import cn.droidlover.xdroidmvp.net.NetError;
import cn.droidlover.xdroidmvp.net.XApi;
import moragame.logicalthinking.moragame.model.HomeResults;
import moragame.logicalthinking.moragame.model.LoginResults;
import moragame.logicalthinking.moragame.net.Api;
import moragame.logicalthinking.moragame.ui.GameRoomActivity;
import okhttp3.RequestBody;

/**
 * Created by wanglei on 2016/12/31.
 */

public class PGameRoomPager extends XPresent<GameRoomActivity> {




    public void getAddReturnHall(RequestBody body) {
        Api.getLoginService().getAddRoomDataJson(body)
                .compose(XApi.<HomeResults>getApiTransformer())
                .compose(XApi.<HomeResults>getScheduler())
                .compose(getV().<HomeResults>bindToLifecycle())
                .subscribe(new ApiSubscriber<HomeResults>() {
                    @Override
                    protected void onFail(NetError error) {
                        getV().showError(error);
                    }

                    @Override
                    public void onNext(HomeResults gankResults) {
                        getV().onAddReturnHall(gankResults);
                    }
                });
    }

    public void updateScore(RequestBody body) {
        Api.getLoginService().getUpdateScoreOutDataJson(body)
                .compose(XApi.<LoginResults>getApiTransformer())
                .compose(XApi.<LoginResults>getScheduler())
                .compose(getV().<LoginResults>bindToLifecycle())
                .subscribe(new ApiSubscriber<LoginResults>() {
                    @Override
                    protected void onFail(NetError error) {
                        getV().showError(error);
                    }

                    @Override
                    public void onNext(LoginResults gankResults) {
                        getV().updateScore(gankResults);
                    }
                });
    }

}

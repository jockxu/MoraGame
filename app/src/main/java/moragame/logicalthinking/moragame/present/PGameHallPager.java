package moragame.logicalthinking.moragame.present;


import cn.droidlover.xdroidmvp.mvp.XPresent;
import cn.droidlover.xdroidmvp.net.ApiSubscriber;
import cn.droidlover.xdroidmvp.net.NetError;
import cn.droidlover.xdroidmvp.net.XApi;
import moragame.logicalthinking.moragame.model.GameHallResults;
import moragame.logicalthinking.moragame.model.HomeResults;
import moragame.logicalthinking.moragame.net.Api;
import moragame.logicalthinking.moragame.ui.GameHallActivity;
import okhttp3.RequestBody;

/**
 * Created by wanglei on 2016/12/31.
 */

public class PGameHallPager extends XPresent<GameHallActivity> {


    public void getAllHall() {
        Api.getLoginService().getQueryAllRoomInfoToGet()
                .compose(XApi.<GameHallResults>getApiTransformer())
                .compose(XApi.<GameHallResults>getScheduler())
                .compose(getV().<GameHallResults>bindToLifecycle())
                .subscribe(new ApiSubscriber<GameHallResults>() {
                    @Override
                    protected void onFail(NetError error) {
                        getV().showError(error);
                    }

                    @Override
                    public void onNext(GameHallResults gankResults) {
                        getV().onAllHall(gankResults);
                    }
                });
    }


    public void getAddReturnHall(RequestBody body) {
        Api.getLoginService().getAddRoomDataJson(body)
                .compose(XApi.<HomeResults>getApiTransformer())
                .compose(XApi.<HomeResults>getScheduler())
                .compose(getV().<HomeResults>bindToLifecycle())
                .subscribe(new ApiSubscriber<HomeResults>() {
                    @Override
                    protected void onFail(NetError error) {
                        getV().showError(error);
                    }

                    @Override
                    public void onNext(HomeResults gankResults) {
                        getV().onAddReturnHall(gankResults);
                    }
                });
    }


}

package moragame.logicalthinking.moragame.present;


import cn.droidlover.xdroidmvp.mvp.XPresent;
import cn.droidlover.xdroidmvp.net.ApiSubscriber;
import cn.droidlover.xdroidmvp.net.NetError;
import cn.droidlover.xdroidmvp.net.XApi;
import moragame.logicalthinking.moragame.model.LoginResults;
import moragame.logicalthinking.moragame.net.Api;
import moragame.logicalthinking.moragame.ui.JLoginActivity;
import okhttp3.RequestBody;

/**
 * Created by wanglei on 2016/12/31.
 */

public class PLoginPager extends XPresent<JLoginActivity> {


    public void login(RequestBody body) {
        Api.getLoginService().getLoginDataJson(body)
                .compose(XApi.<LoginResults>getApiTransformer())
                .compose(XApi.<LoginResults>getScheduler())
                .compose(getV().<LoginResults>bindToLifecycle())
                .subscribe(new ApiSubscriber<LoginResults>() {
                    @Override
                    protected void onFail(NetError error) {
                        getV().showError(error);
                    }

                    @Override
                    public void onNext(LoginResults gankResults) {
                        getV().loginData(gankResults);
                    }
                });
    }


    public void reg(RequestBody body) {
        Api.getLoginService().getRegDataToPostJson(body)
                .compose(XApi.<LoginResults>getApiTransformer())
                .compose(XApi.<LoginResults>getScheduler())
                .compose(getV().<LoginResults>bindToLifecycle())
                .subscribe(new ApiSubscriber<LoginResults>() {
                    @Override
                    protected void onFail(NetError error) {
                        getV().showError(error);
                    }

                    @Override
                    public void onNext(LoginResults gankResults) {
                        getV().regData(gankResults);
                    }
                });
    }


    public void initScore(String id) {
        Api.getLoginService().getInitScoreToGet(id)
                .compose(XApi.<LoginResults>getApiTransformer())
                .compose(XApi.<LoginResults>getScheduler())
                .compose(getV().<LoginResults>bindToLifecycle())
                .subscribe(new ApiSubscriber<LoginResults>() {
                    @Override
                    protected void onFail(NetError error) {
                        getV().showError(error);
                    }

                    @Override
                    public void onNext(LoginResults gankResults) {
                        getV().initScore(gankResults);
                    }
                });
    }
}

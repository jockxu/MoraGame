package moragame.logicalthinking.moragame.utils;

import android.content.Context;

import cn.droidlover.xdroidmvp.cache.SharedPref;

/**
 * Created by Administrator on 2017/4/20.
 */

public class SharedPrefUtils {


    // 得到登录的用户ID
    public static String getLoginID(Context context) {
        return SharedPref.getInstance(context).getString("loginId","0");
    }

    // 设置登录的用户ID
    public static void setLoginID(Context context, String uID) {
        SharedPref.getInstance(context).putString("loginId", uID);
    }


}

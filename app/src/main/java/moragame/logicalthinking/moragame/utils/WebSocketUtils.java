package moragame.logicalthinking.moragame.utils;


import android.os.Handler;
import android.os.Message;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;


import cn.droidlover.xdroidmvp.log.XLog;
import moragame.logicalthinking.moragame.net.Api;
import moragame.logicalthinking.moragame.ui.GameRoomActivity;

/**
 * Created by Administrator on 2017/4/20.
 */

public class WebSocketUtils extends WebSocketClient {


    Handler mHandler;

    public WebSocketUtils(Handler handler) throws URISyntaxException {
        super(new URI(Api.WEBSOCKET_URL));
        this.mHandler = handler;
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {

        Message msg = new Message();
        msg.what = GameRoomActivity.IS_OPEN;
        msg.obj = "open";
        mHandler.sendMessage(msg);
    }

    @Override
    public void onMessage(String message) {

        Message msg = new Message();
        msg.what = GameRoomActivity.START_INFO;
        msg.obj = message;
        mHandler.sendMessage(msg);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {


        Message msg = new Message();
        msg.what = GameRoomActivity.IS_OPEN;
        msg.obj = "close";
        mHandler.sendMessage(msg);
    }

    @Override
    public void onError(Exception ex) {
        XLog.d("连接错误" + ex.toString());
    }

}
